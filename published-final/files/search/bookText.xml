﻿<?xml version="1.0" encoding="utf-8"?><Search><pages Count="16"><page Index="1" isMAC="true"><![CDATA[        The SMP Alliance internal magazine | Issue 1, August 2021
We are one
Celebrating our first year
  building better, together
 ]]></page><page Index="2" isMAC="true"><![CDATA[ Welcome!
Contents
03 Message from Peter Mumford
A personal message from Peter Mumford, Executive Director of Major Projects & Capital Portfolio Management, Highways England.
04 Recognising our major achievements
A review of the SMP Alliance’s key successes in its first year.
10 Building better, together
A specially-commissioned illustration that celebrates the SMP Alliance.
12 Unleashing fresh thinking
Tony Slater, Alliance Manager reflects on how the SMP Alliance has broken the mould and the exciting opportunities ahead.
14 A kaleidoscope of collaboration
A celebration of our people, diversity and togetherness.
16 We are one team from the inside out
Who we are and what we’re all working hard to achieve.
John Grimm
Deputy Alliance Manager
The last year has flown by, but
I can still vividly remember the first day of mobilisation. I’d spent the previous year thinking and planning the activities that we would undertake to start us on our journey, only to then have
to rip most of them up and start again a few weeks before when
it became clear that we would need to mobilise virtually.
I barely knew how to use Microsoft Teams, as I’d only been given a whistle stop tour a couple of days before. I knew practically nobody on our first call and there were little issues (some actually quite big!) that still needed to be resolved.
Yet we worked through the challenges during those early
days and weeks as a team, and
I’m proud to say that ‘can-do’ and collaborative attitude has continued throughout the last year.
I’m often asked whether the SMP Alliance is where I expected it to be a year in and my response is no, we’re not..... we are further ahead!
We have achieved so much and writing this message has given me a rare opportunity to pause and reflect on what we’ve done together, including:
■ Established our vision, mission, values and our six Alliance outcomes
■ Created the Business Realisation Plan (BRP) to map out how we’ll transform our ways of working to realise our ambition
■ Achieved our Year 1 Performance Table objectives
■ Flexed our programme to support the actions from Stocktake anniversary report
■ Established our supply chain strategy and strategic frameworks are now being put in place
■ Developed and launched our new M365 collaboration environment.
On top of that, the Infrastructure Client Group (ICG) were commissioned to undertake
a maturity assessment of the SMP Alliance a few months ago, and I’m very pleased to say that we achieved the highest recorded score with no
critical issues identified.
I know the last year has been challenging for us all for so many reasons, so I would like to say a huge thank you to everyone who has contributed to making our first year such a success.
THANK YOU!
©2021. Connec7 is published by the SMP Alliance Communications Team.
Neelam Sheemar – Head of Communications, Neal Anderson – Senior Internal Communications Consultant, Jane Eccles – Internal Communications Consultant, Victoria Lewis – Internal Communications Consultant, Floriane Winnicott – Internal Communications Officer. Contact: communications@smpalliance.co.uk
2 building better, together
]]></page><page Index="3" isMAC="true"><![CDATA[Message from Peter Mumford
Watch this specially recorded video with Peter Mumford, Executive Director of Major Projects & Capital Portfolio Management at Highways England, who wanted to send a personal message to all members of the SMP Alliance on our first anniversary
Issue 1, August 2021 3
]]></page><page Index="4" isMAC="true"><![CDATA[ 4
building better, together
Recognising our major achievements
Our first anniversary presents us with the perfect opportunity to reflect, recognise and celebrate what we together as the SMP Alliance have accomplished and just how far we have come within 12 months. We examine three key achievements from each of our workstreams
■
■
■
No barriers - success will be achieved through constant interaction and alignment of people and functions. An alignment workshop with all the Scheme Leadership teams and Production Hub functional leads was held in February 2021. This interactive session was a significant step forward in clarifying PMO functions and how the Alliance PMO aligns with the HE SMP PMO team. Feedback from Schemes on support they need brought the PMO and Schemes into closer alignment, with monthly meetings now strengthening connections
Doing the right thing by recognising that Alliance contract deliverable and set-up was not happening as quickly as needed. This was addressed by the Business Mobilisation team with the single focus of driving completion set-up, putting in place the foundation elements needed to enable the Alliance to function as its own entity
Inspiring people to give their best and contribute to a collaborative mindset, especially when the delivery model is new and a significant shift from previous models. PMO leads have regular 1-to-1 meetings with HE counterparts. This has led to monthly Alliance PMO / HE SMP PMO alignment meetings and a PMO transition plan. An overall mission statement focuses PMO leads on transitioning traditional Client PMO activities to the Alliance PMO - a huge step forward in driving new behaviours
PMO
Mark Stanton
PMO Director
Customer
Paul Vause
Head of Customer
■ Delivered customer induction to over 500 members of the SMP Alliance, covering 95% of the Production Hub and Scheme level leadership, setting out the importance of the customer objective, what influences satisfaction and how colleagues can make a positive contribution
■ Successfully delivered our year 1 customer performance measures, building our products and processes to support the Confident Customer outcome. We are
now working towards year 2 performance measures
■ Launched the Customer Working Group, working collaboratively with all partners to build our customer service culture and strengthen the relationship between Production hub and Scheme delivery
]]></page><page Index="5" isMAC="true"><![CDATA[Issue 1, August 2021 5
■
■ ■
Achieved Year 1 performance objectives (as shown below).
Our audit preparation work has been submitted for review and sign off, including evidence to support performance scores. This is a great achievement, and we’re already building on it for Year 2
PMO Performance & Reporting
Steve Shannon
Head of Performance & Reporting
Katie Palmer Risk Lead
Supply Chain
■
■
■
Developed common procurement focused on value not cost, and removed barriers to entry by advertising 100% of our programme level strategic opportunities. Over 70%of planned procurement is now in flight, and we’re appointing / allocating works to our supplier network. As a result, new suppliers are being introduced to the highways sector and smart motorway programme
Transforming how and when we engage with our supplier network. Key supplier personnel have been directly appointed into the Production Hub and we’re engaging with our network as early as possible in the project lifecycle.We’ve seen positive responses from both our suppliers and Alliance colleagues as we make real strides towards a true enterprise
Implementing a transformational and market-leading procurement / supply chain model and have already embedded over 40 strategic supply partners
Operational Dashboard for the board being completed. This forms part of our developing Alliance Wide reporting framework
We’ve had positive conversations with HE regarding the ‘Drumbeat’ and changes to allow more timely internal reviews. We are engaging with key stakeholders to clearly define what these changes will be, and good progress has been made
OBJECTIVE
Safety Customer Delivery
TARGET ACHIEVED SCORE SCORE
143 210 68 71 40 40
OBJECTIVE % GOAL FEE SCORE ADJUSTMENT
147% 0% 104% 0% 100% 0%
PMO Risk Management
■ Rolling the Risk Management
and Early Warning process out to schemes and acting as a conduit in the facilitation between the schemes and the Alliance Manager
■ Successfully taken ownership of 9 Xactium risk registers
■ Commencing the roll out of QRA supported by the HE Major Projects Central QRA Team
Chris Hickey
Head of Supply Chain
]]></page><page Index="6" isMAC="true"><![CDATA[ Transformation
■
■
■
Performance targets necessary to track our ten-year journey have been identified and agreed for the whole SMP Alliance and on an individual Scheme level within the Business Realisation Plan (BRP). These targets track how the BRP initiatives are delivering on the KPI’s associated with
our six Outcomes
To responsibly manage the investment in the BRP, a governance body called the Investment Group has reviewed the first of the investment Business Cases. Importantly they are the custodians of the annual spend sanctioned by the Alliance Board, and are tasked with monitoring expenditure to ensure it stays within governed limits
The BRP rollout and communication plan commenced in June, which includes The BRP video that you can watch here
Neilan Perumal
Transformation Lead
■ Delivering an accelerated Stopped Vehicle Detection (SVD) programme with minimised customer impact. By standardising and simplifying infrastructure design, we have reduced cost, increased efficiency and added flexibility by using pre-cast bases with passive posts, utilising existing gantries and single shallow ducting. We delivered the first scheme (M20) on time, and have safely mobilised a further five schemes since
March 2021
■ The M6 is the first All Lane Running (ALR) scheme mobilised under the SMP Alliance. J21a to J26 is fully mobilised and delivering. Drainage and preparation works to facilitate the concrete safety barrier are underway. The project has a Value Engineering team in place to further refine the existing design with good progress made. We are benefiting from learnings from the M4 scheme to ensure improved efficiencies
■ Achieved great solutions using hackathons. Two examples are: M6 works, where we brought together blended Partner and Supply Chain teams to look at the verge and central reserve. Solutions to save programme time without compromising safety are now used on the M6, and will be embedded in other schemes; Site set-up, where we’ve worked towards ensuring
a standard, scalable, sustainable and inspiring workplace with a focus on logistics and industrialised construction for all schemes
Production
Alistair Geddes
Production Director
6 building better, together
]]></page><page Index="7" isMAC="true"><![CDATA[■ By establishing the Alliance planning community, we have streamlined our planning efforts. Notable successes include: Scheme programme governance and assurance process established, with regular monthly Scheme-level reviews and
a standardised template; HE Planning dashboard templates developed, with successful WD-3 programme reviews involving Heads of Production, Scheme and the ALT; Rules of credit developed and agreed for assessing DED progress, now being utilised for stage 3-5 across all Schemes
■ Collaboratively aspired to improve Scheme reporting to both our client and the ALT. Challenges have been overcome to ensure more consistent data and minimal effort from project teams. Successes include: Hand-over/alignment with HE SMO PMO team and Alliance Reporting team; Alliance Internal reporting matrix defined, stakeholder alignment, and Alliance Reporting Plan issued for review; WD Project Performance Reviews (PPR) taking place; Workflow processes established with correct communication channels and responsible
roles defined
■ We established and agreed the Hub budget for FY21/22, feeding the business plan, and enabling visibility of programmatic functions across the SMP Alliance
PMO Project Controls
Andy White
Hub Commercial & Project Controls Lead
Commercial & Procurement
Peter Winnicott
C&P Director
■ Established the cost and financial systems, processes and governance controls to manage a business projected to grow from zero to £450m turnover in 4 years
■ Successfully used the innovative Alliance Budget Pricing Model to agree £1.4bn of Works Orders, ahead of completing detailed design, and are well on our way to agreeing
another £250m
■ Procured and implemented the contractual management system CEMAR for the first time on an NEC 4 Alliance Contract to manage the complex contractual relationships between the Client, Alliance Manager and Partners Schemes
Issue 1, August 2021 7
■ Since July 2020, through the Quality forum, all partner Quality Managers are working together and meeting weekly to discuss, align, update and work on the Quality deliverables, Quality oversight of Scheme activities, Quality System elements of the new Alliance Integrated Management System (AIMS), Quality Strategy and HE feedback and information among other topics
■ Client alignment towards changing Quality perception, setting the foundations of the ‘Right First Time’ culture in the Alliance. Weekly alignment sessions with HE SMP Head of Quality to define a common Quality vision, purpose and shared values, understand past challenges and obtain regular client feedback
■ Technical Assurance (TA) design approach defined and TA Lead appointed. TA purpose is to assure the SMP Alliance and HE that the design is developed in line with Alliance Design Criteria and Design Rules, and that Construction Scope meets the Client’s Work Specification and Scheme Requirements. The defined TA design approach is being implemented and work is in progress to adapt / evolve this to support schemes in construction phase
PMO Quality
Cecilia Suarez-Lledo
Head of Quality
]]></page><page Index="8" isMAC="true"><![CDATA[ Communications ■ ■
Established a Communications Working Group including representatives from Customer, Highways England Communications and Stakeholder / Comms Lead from each Scheme to ensure that we are aligned, sharing best practices and identifying opportunities
Created a Partner Communications Group to celebrate success, build reputation, ensuring consistent messaging and support the SMP Alliance internally and externally
Internal communications strategy produced and being implemented, along with procedures and BRP initiatives. Together this provides the framework to ensure effective, integrated on-going communications that drive behaviours and engagement. We are also optimising and increasing our channels to ensure efficient delivery and cut through so key messages are noticed, accepted, understood and aligned
Neelam Sheemar
Head of Communications
■ Have submitted and received approval to deliver the dedicated SMP Alliance Microsoft 365 collaboration environment. Although this uncovered complex challenges, we are treating these as opportunities to help streamline
how we will manage future challenges
■ Released the Information Risk Assessment (due within four weeks of award) and Information Security Plan (due within three months of award) within tight timeframes. This brought together our Alliance partner IT security teams
■ Worked hard to communicate and simplify the Baseline Personnel Security Standard (BPSS) requirements, while building a trusted relationship with our client’s security team. This has underpinned establishing user access to a number of Highways England mandated systems that demand BPSS
PMO Systems & Technology
Stuart McLean
Head of Systems Integration and Technology
Health Safety & Wellbeing
■ Set our Business Realisation Plan (BRP) objectives and roadmap for the SMP Alliance
■ Established our team, our identity, the wider SMP Alliance interfaces and the plan for our management system
■ Achieved our Year 1 Performance objectives and set our Year 2
Sinéad Brown
Head of HSWE
8 building better, together
■
]]></page><page Index="9" isMAC="true"><![CDATA[Sneeta Madhara
Recruitment Manager
■
■
■
Created blended teams by bringing diverse talent together from across the seven Partners
Embedded a fair, equitable recruitment process that
is enabling the SMP Alliance to identify the best available talent
Our ‘Onboarding’ process has been developed so new hires will be set up for success
People
PMO Digital & Information Management
Adrian Malone
Head of Digital and Information Management
■ Working collaboratively with the Alliance Members, we’ve co-created an ambitious digital strategy and plan that has influenced the Alliance’s Business Realisation Plan (BRP). This provides a strong and shared foundation for building our digital capability and culture
■ Achieving digital ‘Brilliant Basics’ to support consistency
in data and information management and delivery across schemes. This is about understanding how we can align the processes, tools and systems that we all use at a partner level, and identifying how to transform so they work best whilst we’re working as one Alliance – a challenge within such a complex organisation
■ Shaping certain critical Alliance processes, including: Determining how to assess digital competency (and undertaking those assessments), both for the Alliance
and the Supply Chain, which includes collaboration with Commercial; Collaborating with PMO - Systems and Technology to support the roll out of Microsoft 365 and determining the team / channel structure prior to migration from the Highways England’s SharePoint / Teams to our own dedicated site
Issue 1, August 2021 9
■ ■ ■
Creating our ‘25 by 25’ carbon road map, which will support Highways England’s carbon ambitions
Fully leveraging progress to date by retaining knowledge and systems from the Collaborative Delivery Framework (CDF)
Establishing the carbon and environment business plan, and obtaining full governance support for delivery
Environment & Carbon
Jamie Bardot
Environment and Carbon Lead
]]></page><page Index="10" isMAC="true"><![CDATA[  1
10
0 b
n in
b
e
t
r er
bu
og
u
ld
il
g b
he
i
l
di
tt
d
i
n
g
b
et
, t
e
t
t
te
r
ge
e
r
,
t
o
g
et
e
th
t
h
e
r
]]></page><page Index="11" isMAC="true"><![CDATA[ I
1
Is
s
s
su
uIs
e
es
1
1u,
,
eA
A
1
u
u,
g
gJ u
u
u
s
slyt
t
2
2
0
0
2
2
1 1
21
1
1
1
]]></page><page Index="12" isMAC="true"><![CDATA[ Unleashing fresh thinking
Alliance Manager Tony Slater shares his reflections on how the SMP Alliance has broken the mould in the past 12 months and also what exciting opportunities lie ahead
 Tony Slater joined Highways England last year to lead the SMP Alliance, bringing a wealth of infrastructure and transport knowledge skills, including collaborative contracting experience. He was immensely excited to join as Alliance Manager, as he saw this as a unique chance to put into practice his forward-thinking vision of how our highways are developed for the future.
Commenting on his first weeks into his new role, Tony reveals: “The early weeks and months of any project, never mind a comprehensive programme of this size, often bring difficulty and trepidation. What none of us had anticipated was the SMP Alliance had the added complexity of having to mobilise during a global pandemic.
“This raised a lot of questions. Could we keep what is a very aggressive
programme on some sort of track? Could we mobilise remotely over Microsoft
Teams and using limited resources, or would we have to defer?
We also had so many unknowns, such as when lockdown would end, which
made planning an immense challenge. However, we still had to make some very
important decisions that could hugely implicate the mobilisation of our programme. We just got on with it.
“I’m pleased we did, and I am immensely proud of the work the whole team has managed to do in a virtual world with limited people and resources on-site. What this clearly demonstrates is that we have the capabilities to really drive the Smart Motorways Programme forward.”
 I am immensely proud of the work the whole team has managed to do in a virtual world
 12 building better, together
]]></page><page Index="13" isMAC="true"><![CDATA[Tony went on to highlight the unique challenges of the SMP Alliance, saying: “Bringing seven different organisations together who are, when it comes down to it, direct competitors, has potential for friction and lack of cohesion. I asked myself could we really collaborate and get unification amongst all the Partners and the people we’re bringing together?
The answer is an emphatic yes!
Exceeding expectations
“As someone watching everything evolve within each area of the SMP Alliance, I’ve been amazed at how well everyone is integrating and collaborating. Even within these unprecedented circumstances brought about by COVID-19,
we’re going far deeper in terms of integration and collaboration than we could have hoped for. There are still inevitable bumps in the road, but everyone is making a commitment to finding efficient and innovative solutions to make this a success.
I’ve been amazed at how well everyone is integrating and collaborating
“For example, on the Stopped Vehicle Detection (SVD) programme we’ve designed work in record
time and on-site we are performing extremely well. We’re looking at the dynamic hard shoulder to All Lane Running (ALR) programme again, taken that through the design stages, and we’re about to take that on-site to construction. Two very difficult and tricky pieces of work that have set a new benchmark in terms of getting the design phases complete and mobilising on-site.
“Seeing our performance excel in this way, and our resilience to continuous change, as well as how we are collaborating and strengthening our relationship with Highways England, is just incredible and has exceeded expectations. This makes me even more enthused to keep pushing forward to make the SMP Alliance the best it can be over the next
year and beyond.”
Courage and conviction
Tony then underlined how the SMP Alliance values: We are brave; We do the right thing; We deliver the digital roads of the future are the backbone to everything we do, saying: “When
I think about how we’ve been brave,
I see the excellent development of the Project Control Framework (PCF) products and documents, and also how each scheme is breaking down company barriers to help each other. Whether it’s the design team or the on-site assembly teams, we have put natural competitors together in the same environment, all focussing on the same goal.
“I recognise that it’s not easy to break down company boundaries - it can be tough when you’re in a business to offer help to a competitor, and indeed it can be just as tough to accept it. Yet working in the SMP Alliance I have seen many examples of the design teams helping each other - WSP helping Jacobs and visa-versa, Balfour Beatty, Costain and the BAM / Morgan Sindall JV
are all supporting each other.
“To do this, to share and collaborate, takes huge bravery, as well as the right people with the courage and conviction to do the right thing. Being brave and doing the right thing go hand in hand. I believe that being brave is about being able to break away or work around aspects that block or get in the way of us doing the right thing, and it is clear that within the SMP Alliance we are on the right path to do just that.
Making a difference
Looking ahead, Tony said: “Of course a programme of this scale brings
its hurdles, but it also brings huge excitement and awe around what we can do together and what we are doing for the future of our highways network.
“We have set ourselves a really ambitious plan of achieving great performance in six outcomes, which I see as all to being as important
as each other. So, if we focus
on resilient frontier businesses, flourishing environment, having an inspiring workplace, home safe and well, confident customers and enriched communities, and all the measures that support these, we will outperform. That’s exciting.
What greater legacy than being part of this?
“We have a plan, we have targets, we have shared them with everybody to achieve that. I really want to ask people to get behind these, get that collective desire and let’s go and smash these outcomes.
“The success that will come for everybody that’s part of this team will just be amazing. At the minute, the SMP Alliance is seen as a bit of a ‘newbie’ on the block. We’re quite new and novel, people are looking at us to see how we perform, and I know we can as I’ve already seen it. This really excites me.
“We are building roads for vehicles that don’t exist yet. We are building something that will leave a lasting positive impact on how people and businesses move around England.
“We are building the digital roads of the future. We can stand back and say ‘I’ve been part of that’, ‘I built that’, ‘I was involved in that’. What greater legacy than being part of this? That’s just incredible.”
     IssuIses1u,eA1u, gJuslyt 2021 13
We do the right thing
in all weathers
]]></page><page Index="14" isMAC="true"><![CDATA[ A kaleidoscope of collaboration
Embracing differences, no matter what they are, and welcoming innovative thinking
The SMP Alliance is creating a truly inclusive and collaborative environment where all colleagues feel welcome, empowered to be themselves and valued (no matter what their background, level, parent company or location) to share their experiences and provide their own inspirational contribution to transform delivery and meet customers’ needs.
We value and are committed to supporting diversity in every sense. Not only is this right, it is because we appreciate that diverse perspectives, experiences and backgrounds add comprehensive depth to strengthen
the SMP Alliance and enable it to be more innovative and dynamic.
We’re on a ten-year journey, and in our first year we’ve already made great strides in implementing our Equality, Diversity and Inclusion (EDI) Strategy at the heart of what we do. This deserves to be recognised and celebrated, so we spoke to some wonderful colleagues to get their views
on how we’re building better, together.
 Grace Warre   Neilan Perumal
Graduate ProManager
 “Working during COVID-19 has
brought a new dimension to colleague relationships,
as we now we allow people into our homes daily through Teams calls.
“Through this we’ve gained a deeper understanding of colleagues outside work, and sometimes the challenges this brings. This ‘intrusion’ has forged deeper relationships, more respect and greater collaboration because we appreciate each other on a more personal level and the responsibilities and sacrifices we make at home.
“This inclusivity within a team can only support the SMP Alliance with more flexible working patterns, diverse backgrounds and more agile places of work. I see this already occurring, and hope it continues.”
Transformation Lead
“I have a goosupport netwwhich providesme with the confidence to makedecisions day to day on the M40M42 interchange. I know those around me are there for guidancif needed and being encouragedto take on responsibility and takeownership of the tasks I am givefacilitates my growth in both my career development and persondevelopment.”
   Watch for more thoughts on diversity
add Georgi Razvans Site Engi“Being in a diverse treally helps to feel part of something big. amazing how so many peoplework as a team and collaboradeliver high quality projects.”
  14 building better, together
]]></page><page Index="15" isMAC="true"><![CDATA[ Mark Stanton
 “What has impressed me
most is that we’re moving past
‘collaborating as seven Partners’ to really be one SMP Alliance with a focus on delivering our Schemes in a new and more efficient way.
“We still have a way to go on this journey, and there will be more twists and turns, but in the last
12 months we should be hugely proud that we started in the middle of a pandemic and yet still came together remotely to form the SMP Alliance and take our vital first steps to transform delivery.”
Dimitrios Zavos
PMO Director
 PMO Lead M25
n
“Even from the very first
days of my assignment on
the M25 Scheme, the potential and encouragement to make a difference is pretty evident. I’m embracing this opportunity to put my organisational skills and eye for detail to good
use in order to reinforce an already successful team.”
Our collaborative culture is underpinned by our agreed behaviours: Decision making; Accountability; Communication and engagement; Innovation and improvement; Constructive challenge; Trust and Respect.
Inclusion and diversity are core attributes to our identity, and our EDI plan is in place. Employees feel engaged, connected, valued and respected for who they are. For us, this means creating a culture of belonging where we all thrive by embracing all perspectives.
  ject Tolani Azeez
Lead (to June 2021)
d ork,
 
/ e   n, al Business Mobilisation
 Justine Jefferies
“The Business Mobilisation Panel’s most productive,
interesting and innovative meetings were those with cross-Partner participation. In those meetings
we had a shared vision, an understanding of the objective of the meeting and everyone had an opportunity to contribute.
“From my SMP Alliance experience, three areas have contributed to success: Transparency; Constant sharing of knowledge and collaboration of great ideas; Respect for everyone in each meeting and a willingness to listen and learn from each other.”
Head of People
 Adrian Malone
 “When I think about the
ambition of the SMP Alliance, the opportunity that’s been created, the potential which we can harness by bringing together the best of all of our organisations, collaboration is fundamental to it all.
“For me, true collaboration is the small stuff – creating trust, building alignment and finding creative ways to connect a diverse and distributed team so that we co-create better solutions.”
We positively encourage applications from qualified candidates from
all diverse groups. We are committed to enhancing skills diversity as well as ressing gender imbalance. Diversity is key to bringing different insights, creating challenge, and encouraging innovation and transformation within the sector.
Head of Digital & Information Management
Barry Jacks
 ki
ALR Technical Solutions
neer
Owner
Sneeta Madhara
“One of the great things about the
SMP Alliance is the people. The togetherness, dedication and hard work that they put in to ensure successful delivery pushes me to excel even further so that I don’t let anyone down.”
 such eam
me It is
 can te to Recruitment Manager
 “It’s about accepting
everyone for who they are, understanding that
everybody is unique and celebrating each and every one of us for what we bring to the table.”
 Issue 1, August 2021 15
]]></page><page Index="16" isMAC="true"><![CDATA[                                Safety Customer Delivery Safety Ce
D
Ce Dy
Ce D
Ce Dy
Ce D
Ce Dy
Ce D
 ustomer Delivery Safety Custom
Delivering the
elivery Safety Customer Delivery
digital roads of
Safety Customer Delivery Safety
 Working towards six outcomes:
the future
1. Home safe and well
ustomer Delivery Safety Custom
elive
y Safety Customer Deliver
2. Inspiring workspaces
3. Resilient frontier business
r
 4
.
Collaborating across nine workstreams:
 6. Enhancing local environments
Safety Customer Delivery Safety
5. Enriching communities
 ustomer Delivery Safety Custom
elivery Safety Customer Delivery
    Commercial Communications Customer Production People
     Safety Customer Delivery Safety
Uniting diverse teams,
Supplier Network PMO Transformation HSWE
unleashing fresh thinking and transforming delivery
ustomer Delivery Safety Custom
 elivery Safety Customer Deliver
elivery Safety Customer Delivery Safety Customer Delivery Safety
ustomer Delivery Safety Custom elivery Safety Customer Deliver
Safety Customer Delivery Safety ustomer Delivery Safety Custom
Inspiring each other every day to be ever more innovative and
successful in delivery
Safety Customer Delivery Safety ustomer Delivery Safety Custom
We are...
      elivery Safety Customer Delivery
          Safety Customer Delivery Safety
]]></page></pages></Search>